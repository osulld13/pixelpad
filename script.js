var starting_dimensions = 50;

//grid properties
var current_dimensons;
var grid_size = 500;

$(document).ready(function(){
	//add button handlers
	clear_button_press();
	button_hover();

	//initial setup
	setup_grid(starting_dimensions);
});

//a function that sets up a grid to a given size
function setup_grid(dimensions_in){
	current_dimensons = dimensions_in;
	//clear the grid
	clear_grid();
	//add new grid
	new_grid();
	//a nested loop to initialise the grid to the given size
	for(var i = 1; i <= current_dimensons; i ++){
		//create the string for the new cell and append to Document body
		var new_row = "<div class=\"row\"></div>";
		$('#grid').append(new_row);
		// loop to populate each row with a cell
		for(var j = 1; j <= current_dimensons; j ++){
			var new_cell = "<div class=\"cell\"></div>"
			//get last row so cells can be appended to it
			var last_row = $('.row:last-child');
			last_row.append(new_cell);
		}
		//style inserted cells
		style_cells();
	}
	//style the inserted rows
	style_rows();

	//call the draw function
	draw();
}

//a function to clear the grid
function clear_grid(){
	$('#grid').remove();
}

//function to create a new grid
function new_grid(){
	//create a new string for grid, append to doc and add relevant stylings
	var new_grid_element = "<div id=\"grid\"></div>"
	$('#grid_wrapper').append(new_grid_element);
	$('#grid').css({
		"height": grid_size+"px",
		"width": grid_size + "px"
	});
}

function style_rows(){
	var row_height = grid_size / current_dimensons;
	//style new rows
	$('.row').css({
		"height": row_height+"px",
		"width": grid_size+"px"
	});
}

function style_cells(){
	//cell height is the same as its rows height
	var cell_size = grid_size / current_dimensons;
	//style new rows
	$('.cell').css({
		"height": cell_size+"px",
		"width": cell_size+"px",
		"display": "inline-block"
	});
}

//function that enables carries out operations fo drawing
function draw(){
	$('.cell').hover(function() {
		$(this).css({
			"background-color" : "#404040"
		})
	});
}

function button_hover(){
	$("#clear_button").hover(function() {
		$(this).css("background-color", "#4477EE");
	}, function() {
		$(this).css("background-color", "#6699FF");
	});
}

function clear_button_press(){
	$("#clear_button").mousedown(function(){
		$(this).css("background-color", "#88BBFF");
	});
	$("#clear_button").mouseup(function(){
		$(this).css("background-color", "#6699FF");

		do{
			var new_size = prompt("Enter the size of the new PixelPad", "50");
		}while(isNaN(new_size));

		setup_grid(new_size);
	});
}